# ======
# Documentation
# ======

# https://docs.gitlab.com/ee/user/project/code_owners.html

# Ending a path in a `/` will specify the code owners for every file
# nested in that directory, on any level

[Default]
* @jjstark @kathleentam @tayloramurphy @m_walker @paul_armstrong


[Admin]
/.gitlab/       @kathleentam    @jjstark    @tayloramurphy
/admin/         @kathleentam    @jjstark    @tayloramurphy
/analyze/       @kathleentam    @jjstark    @tayloramurphy

CODEOWNERS      @kathleentam    @jjstark    @tayloramurphy
.gitignore      @kathleentam    @jjstark    @tayloramurphy
CONTRIBUTING.md @kathleentam    @jjstark    @tayloramurphy
LICENSE.md      @kathleentam    @jjstark    @tayloramurphy
README.md       @kathleentam    @jjstark    @tayloramurphy
__init__.py     @kathleentam    @jjstark    @tayloramurphy


[Data Engineering]
/dags/              @tayloramurphy  @m_walker   @paul_armstrong
/extract/           @tayloramurphy  @m_walker   @jjstark          @paul_armstrong
/load/              @tayloramurphy  @jjstark    @kathleentam
/orchestrate/       @tayloramurphy  @m_walker   @paul_armstrong

Makefile            @tayloramurphy  @m_walker   @paul_armstrong
docker-compose.yml  @tayloramurphy  @m_walker   @paul_armstrong
.gitlab-ci.yml      @tayloramurphy  @m_walker   @paul_armstrong



# ======
# dbt Overwrites
# ======

[dbt]
/transform/                                         @tayloramurphy  @jjstark        @kathleentam
/transform/general/                                 @tayloramurphy  @kathleentam    @pluthra
/transform/snowflake-dbt/                           @tayloramurphy  @jjstark        @kathleentam    @m_walker       @paul_armstrong

/transform/snowflake-dbt/snowflake-dbt-ci.yml       @tayloramurphy  @m_walker       @jjstark        @kathleentam    @paul_armstrong
/transform/snowflake-dbt/generate_dbt_schema.py     @tayloramurphy  @m_walker       @jjstark        @kathleentam    @paul_armstrong
/transform/snowflake-dbt/periscope_check.py         @tayloramurphy  @m_walker       @jjstark        @kathleentam    @paul_armstrong

/transform/snowflake-dbt/analysis/                  @tayloramurphy  @jjstark        @kathleentam
/transform/snowflake-dbt/docs/                      @tayloramurphy  @jjstark        @kathleentam
/transform/snowflake-dbt/macros/                    @tayloramurphy  @jjstark        @kathleentam    @paul_armstrong

/transform/snowflake-dbt/models/bamboohr/           @pluthra        @kathleentam    @tayloramurphy
/transform/snowflake-dbt/models/covid19/            @tayloramurphy  @jjstark        @kathleentam
/transform/snowflake-dbt/models/marts/product_kpis/ @mpeychet_      @kathleentam

/transform/snowflake-dbt/models/common/             @jjstark    @paul_armstrong @m_walker   @tayloramurphy
/transform/snowflake-dbt/models/common_mapping/     @jjstark    @paul_armstrong @m_walker   @tayloramurphy

/transform/snowflake-dbt/models/legacy/smau_events/ @mpeychet_      @kathleentam
/transform/snowflake-dbt/models/legacy/snapshots/   @m_walker       @kathleentam    @tayloramurphy
/transform/snowflake-dbt/models/legacy/snowplow/    @tayloramurphy  @mpeychet_

/transform/snowflake-dbt/models/sources/customers/  @mpeychet_		@m_walker       @kathleentam
/transform/snowflake-dbt/models/legacy/customers/   @mpeychet_		@m_walker       @kathleentam

/transform/snowflake-dbt/models/sources/dbt         @m_walker   @jjstark    @tayloramurphy
/transform/snowflake-dbt/models/legacy/dbt          @m_walker   @jjstark    @tayloramurphy

/transform/snowflake-dbt/models/sources/engineering/        @m_walker   @kathleentam    @ken_aguilar
/transform/snowflake-dbt/models/legacy/engineering/         @m_walker   @kathleentam    @ken_aguilar

/transform/snowflake-dbt/models/sources/gitlab_data_yaml/   @tayloramurphy  @kathleentam    @jjstark
/transform/snowflake-dbt/models/legacy/gitlab_data_yaml/    @tayloramurphy  @kathleentam    @jjstark

/transform/snowflake-dbt/models/sources/gitlab_data_yaml/feature_flags_source.sql          @ayufan  @m_walker
/transform/snowflake-dbt/models/legacy/gitlab_data_yaml/feature_flags_yaml_latest.sql      @ayufan  @m_walker
/transform/snowflake-dbt/models/legacy/gitlab_data_yaml/feature_flags_yaml_historical.sql  @ayufan  @m_walker

/transform/snowflake-dbt/models/sources/gitlab_dotcom/      @mpeychet_		@m_walker       @kathleentam       @jeanpeguero
/transform/snowflake-dbt/models/legacy/gitlab_dotcom/       @mpeychet_		@m_walker       @kathleentam       @jeanpeguero

/transform/snowflake-dbt/models/sources/google_analytics_360/   @mpeychet_	@jjstark  @paul_armstrong	@jeanpeguero
/transform/snowflake-dbt/models/legacy/google_analytics_360/    @mpeychet_	@jjstark  @paul_armstrong	@jeanpeguero

/transform/snowflake-dbt/models/sources/greenhouse/ @pluthra        @kathleentam	@tayloramurphy
/transform/snowflake-dbt/models/legacy/greenhouse/  @pluthra        @kathleentam	@tayloramurphy

/transform/snowflake-dbt/models/sources/handbook/   @m_walker       @kathleentam    @ken_aguilar
/transform/snowflake-dbt/models/legacy/handbook/    @m_walker       @kathleentam    @ken_aguilar

/transform/snowflake-dbt/models/sources/license/    @mpeychet_		@m_walker       @kathleentam
/transform/snowflake-dbt/models/legacy/license/     @mpeychet_		@m_walker       @kathleentam

/transform/snowflake-dbt/models/sources/netsuite/   @iweeks         @kathleentam    @tayloramurphy
/transform/snowflake-dbt/models/legacy/netsuite/    @iweeks         @kathleentam    @tayloramurphy

/transform/snowflake-dbt/models/sources/pmg         @jeanpeguero    @paul_armstrong   @jjstark
/transform/snowflake-dbt/models/legacy/pmg          @jeanpeguero    @paul_armstrong   @jjstark

/transform/snowflake-dbt/models/sources/qualtrics   @m_walker       @jjstark     @tayloramurphy
/transform/snowflake-dbt/models/legacy/qualtrics    @m_walker       @jjstark     @tayloramurphy

/transform/snowflake-dbt/models/sources/sfdc/       @kathleentam	@jeanpeguero   @jjstark     @paul_armstrong    @iweeks
/transform/snowflake-dbt/models/legacy/sfdc/        @kathleentam    @jeanpeguero   @jjstark     @paul_armstrong    @iweeks

/transform/snowflake-dbt/models/sources/snowflake/  @m_walker       @kathleentam    @jjstark    @tayloramurphy
/transform/snowflake-dbt/models/legacy/snowflake/   @m_walker       @kathleentam    @jjstark    @tayloramurphy

/transform/snowflake-dbt/models/sources/version/    @mpeychet_		@m_walker       @kathleentam
/transform/snowflake-dbt/models/legacy/version/     @mpeychet_		@m_walker       @kathleentam

/transform/snowflake-dbt/models/sources/zendesk     @ken_aguilar    @kathleentam
/transform/snowflake-dbt/models/legacy/zendesk      @ken_aguilar    @kathleentam

/transform/snowflake-dbt/models/sources/zuora       @iweeks     @tayloramurphy      @paul_armstrong      @jeanpeguero
/transform/snowflake-dbt/models/legacy/zuora        @iweeks     @tayloramurphy      @paul_armstrong      @jeanpeguero

/transform/snowflake-dbt/models/marts/arr           @iweeks         @jeanpeguero    @paul_armstrong      @jjstark
/transform/snowflake-dbt/models/marts/sales_funnel  @iweeks         @jeanpeguero    @paul_armstrong      @jjstark

/transform/snowflake-dbt/models/workspaces/workspace_sales  @nfiguera @mvilain

/transform/snowflake-dbt/models/workspaces/workspace_finance  @statimatla @fkurniadi @james.shen @cmestel @cmachado1 @iweeks

# ======
# dbt data file Overwrites
# ======
/transform/snowflake-dbt/data/cost_center_division_department_mapping.csv               @iweeks     @kathleentam
/transform/snowflake-dbt/data/engineering_productivity_metrics_projects_to_include.csv  @m_walker   @kathleentam    @tayloramurphy
/transform/snowflake-dbt/data/excluded.csv                                              @jjstark    @tayloramurphy  @kathleentam
/transform/snowflake-dbt/data/google_analytics_custom_dimension_indexes.csv             @jjstark    @tayloramurphy  @kathleentam
/transform/snowflake-dbt/data/handbook_file_classification_mapping.csv                  @m_walker   @kathleentam
/transform/snowflake-dbt/data/historical_counts_maintainers_engineers.csv               @m_walker   @kathleentam    @tayloramurphy
/transform/snowflake-dbt/data/netsuite_expense_cost_category.csv                        @iweeks     @kathleentam
/transform/snowflake-dbt/data/projects_part_of_product.csv                              @clefelhocz1
/transform/snowflake-dbt/data/projects_part_of_product_ops.csv                          @clefelhocz1
/transform/snowflake-dbt/data/raw_netsuite_currencies.csv                               @iweeks     @kathleentam    @tayloramurphy
/transform/snowflake-dbt/data/seeds.md                                                  @m_walker   @kathleentam    @tayloramurphy  @jjstark
/transform/snowflake-dbt/data/seeds.yml                                                 @m_walker   @kathleentam    @tayloramurphy  @jjstark
/transform/snowflake-dbt/data/snowflake_contract_rates.csv                              @jjstark    @m_walker       @tayloramurphy
/transform/snowflake-dbt/data/version_usage_stats_to_stage_mappings.csv                 @mpeychet_  @kathleentam
/transform/snowflake-dbt/data/zuora_asc_606_conversion_factors.csv                      @iweeks     @kathleentam
/transform/snowflake-dbt/data/zuora_country_geographic_region.csv                       @iweeks     @kathleentam    @tayloramurphy
/transform/snowflake-dbt/data/zuora_excluded_accounts.csv                               @gitlab-data
